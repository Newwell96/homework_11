import React, {useContext} from "react";
import "./timer.css"
import { observer} from "mobx-react";
import {GameContext} from "../../../../../store/GameStore";

const MyTimer = () => {
    const gameStore = useContext(GameContext);

    return (
        <div id="game-time">
            {gameStore.formattedTime}
        </div>
    );
};

export default observer(MyTimer);
