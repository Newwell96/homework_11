import React from 'react';
import './message-chat.css';

const MyMessage = (
    {
        name,
        messageSide,
        time,
        messageText,
    }
) => {

    let msgStyle = ""
    let sbjStyle = ""

    function getSide () {
        if (messageSide === "X") {
            msgStyle = "msg-container other"
            sbjStyle = "subject-name x"
        }
        else if (messageSide === "O") {
            msgStyle = "msg-container me"
            sbjStyle = "subject-name zero"
        }
    }

    getSide()

    return (
        <div className={msgStyle}>
            <div className="msg-header">
                <div className={sbjStyle}>{name}</div>
                <div className="time">{time}</div>
            </div>
            <div className="msg-body">{messageText}</div>
        </div>
    );
};

export default MyMessage;