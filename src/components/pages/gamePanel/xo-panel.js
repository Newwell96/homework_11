import React from 'react';
import './xo-panel.css';
import MyGameInfo from "./gameInfo/game-info";
import MyChat from "./gameChat/chat";
import MyBoard from "./gameBoard/game-board";


function XOPanel() {

    return (
        <div id="main-container">
            <MyGameInfo/>
            <MyBoard/>
            <MyChat/>
        </div>
    )
}

export default XOPanel;
