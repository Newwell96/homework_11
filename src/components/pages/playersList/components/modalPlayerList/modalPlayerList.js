import React from 'react';
import './modalPlayerList.css';
import MyButton from "../../../../UI components/button/button";
import MyInput from "../../../../UI components/input/input";
import RadioButton from "./radioButton/radioButton";

const MyModalAdd = ({
                        active, setActive, stateList, setStateList,
                    }) => {

    const addPlayer = () => {
        const nameInput = document.getElementById("name");
        const ageInput = document.getElementById("age");
        const genderInput = document.querySelector('input[name="myRadioGroup"]:checked');
        const genderValue = genderInput ? genderInput.value === 'boy' ? 'male' : 'female' : '';
        const now = new Date().toLocaleString('ru', {
            day: 'numeric', month: 'long', year: 'numeric'
        }).replace(/ г\./, '');

        if (nameInput.value!=="" && ageInput.value!=="" && genderValue!=="") {
            const newData = {
                "name": nameInput.value,
                "age": Number(ageInput.value),
                "gender": genderValue,
                "status": true,
                "creatingDate": now,
                "changedDate": now
            };
            const newStateList = [...stateList, newData];
            setStateList(newStateList);
            setActive(false)
        }
    }

    const inputHandler = (event) => {
        const ageInput = document.getElementById("age");

        event.target.value = event.target.value.replace(/[^0-9]/g, "");
    }

    return (<div
            className={active ? "modal active" : "modal"}
        >
            <div
                className={active ? "modal-winner show" : "modal-winner"}>
                <div
                    className="class-close"
                >
                    <MyButton
                        className="class-button"
                        imageName="close"
                        imageWidth={13.18}
                        imageHeight={13.18}
                        onClickFunction={() => setActive(false)}
                    />
                </div>

                <h1
                    className="add-text"
                >
                    Добавьте игрока
                </h1>
                <MyInput
                    label="ФИО"
                    labelClassName="label-name-class"
                    containerClassName="inputLabel-class"
                    className="inputData name"
                    inputName="name"
                    id="name"
                    name="name"
                    type="text"
                    placeholder="Иванов Иван Иванович"
                />
                <div
                    className="age-gender-row"
                >
                    <MyInput
                        label="Возраст"
                        labelClassName="label-name-class"
                        containerClassName="inputLabel-class age"
                        className="inputData age"
                        inputName="age"
                        id="age"
                        name="age"
                        type="text"
                        placeholder="0"
                        onInput={inputHandler}
                    />
                    <div
                        className="gender-container"
                    >
                        <h1
                            className="gender-label-class"
                        >
                            Пол
                        </h1>
                        <div
                            className="radio-set"
                        >

                            <RadioButton
                                name="girl"
                            />
                            <RadioButton
                                name="boy"
                            />
                        </div>
                    </div>

                </div>
                <MyButton
                    className="button background-default-green"
                    buttonText="Добавить"
                    onClickFunction={() => {
                        addPlayer();
                    }}
                />
            </div>
        </div>);
};

export default MyModalAdd;
