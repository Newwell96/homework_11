import {observable, action, makeObservable, computed, reaction, autorun} from 'mobx';
import MyCell from "../pages/gamePanel/gameBoard/components/cell/cell";
import React, {createContext} from "react";
import mockGameApi from "../mocks/gameMock";
import chatData from "../pages/gamePanel/gameChat/data/chatData";
import gameWebSocket from "../mocks/webSocketMock";

class GameStore {
    users = [];
    // chat = chatData.chatList;
    chat = [];

    get firstNameX() {
        return this.playerX?.fio.split(' ')[0];
    }
    get lastNameX() {
        return this.playerX?.fio.split(' ')[1];
    }
    get firstNameO() {
        return this.playerO?.fio.split(' ')[0];
    }
    get lastNameO() {
        return this.playerO?.fio.split(' ')[1];
    }
    get nameX() {
        return this.firstNameX +" "+ this.lastNameX;
    }
    get nameO(){
        return  this.firstNameO +" "+ this.lastNameO;
    }
    squares = Array(9).fill(null);
    xIsNext = true;
    isTimerRunning = true;
    winner = null;
    winnerName = null;
    isGameRunning = true;
    winnerCombination = [];
    modalActive = false;
    seconds = 0;
    intervalId = null;

    constructor() {
        makeObservable(this, {
            squares: observable,
            setSquares: action,

            chat: observable,
            setChat: action,

            xIsNext: observable,
            setXIsNext: action,

            isGameRunning: observable,
            setIsGameRunning: action,

            winner: observable,
            setWinner: action,

            winnerName: observable,
            setWinnerName: action,
            calculateWinnerName: action,

            winnerCombination: observable,
            setWinnerCombination: action,

            modalActive: observable,
            setModalActive: action,

            seconds: observable,
            intervalId: observable,

            formattedTime: computed,
            startTimer: action,
            stopTimer: action,
            resetTimer: action,

            playerO: computed,
            playerX: computed,
            users: observable,
            currentPlayer: computed,
        })

        autorun(
            (startTimer) => {
                this.startTimer();
            }
        );

        reaction(
            () => this.winner,
            () => {
                if (this.winner !== null) {
                    this.stopTimer();
                }
                this.calculateWinnerName();
            }
        );

        reaction(
            () => this.squares,
            () => {
                if (this.squares.includes(null)) {
                } else {
                    this.calculateWinnerName();
                    this.setIsGameRunning(false);
                    this.setModalActive(true);
                }
            }
        );

        reaction(
            () => this.squares,
            () => {
                mockGameApi.sendFieldState(this.squares)
            }
        );

        reaction(
            () => this.chat,
            () => {
                mockGameApi.sendChatMessage(this.chat)
            }
        );
        reaction(
            () => this.xIsNext,
            () => {
                mockGameApi.sendXisNext(this.xIsNext);
            }
        );

        this.initInfo();
        this.initField();
        this.initNext();
        this.initChat();
    }

    get playerO(){
        return this.users[0];
    }
    get playerX(){
        return this.users[1];
    }

    async initInfo() {
        this.setUsers(await mockGameApi.getPlayersInfo());
    }
    async initChat() {
        const chat = JSON.parse(await mockGameApi.getChatMessage());
        if (chat) {
            this.setChat(chat);
    }
    }

    async initNext() {
        const status = await mockGameApi.getXisNext();
        if (status) {
            if (status === "true") {
                this.setXIsNext(true)
            } else if (status === "false") {
                this.setXIsNext(false)
            }
        }
    }

    async initField() {
        const squaresFromStorage = JSON.parse(await mockGameApi.getFieldState());
        if (squaresFromStorage) {
            this.setSquares(squaresFromStorage);
        }
    }

    setChat(newChat) {
        this.chat = newChat;
    }

    setUsers(users) {
        this.users = users;
    }

    get currentPlayer () {
        if (this.xIsNext) {
            return this.playerX
        }
        else {
            return this.playerO
        }
    }

    calculateWinnerName () {
        if (this.playerX.side === this.winner) {
            this.setWinnerName(this.nameX)
        }
        else if (this.playerO.side === this.winner) {
            this.setWinnerName(this.nameO)
        }
        else
            this.setWinnerName("Сопернический дух")
    }

    setSquares(newSquares) {
        this.squares = newSquares;
    };

    setXIsNext(value) {
        this.xIsNext = value;
    };

    setIsGameRunning(isRunning) {
        this.isGameRunning = isRunning;
    };

    setWinner(winner) {
        this.winner = winner;
    };

    setWinnerName(name) {
        this.winnerName = name;
    };

    setWinnerCombination(comb) {
        this.winnerCombination = comb;
    };

    setModalActive(value) {
        this.modalActive = value;
    };

    setSeconds(newSeconds) {
        this.seconds = newSeconds
    };

    handleClick(i) {
        const row = Math.floor(i / 3);
        const col = i % 3;
        gameWebSocket.makeMove(this.xIsNext?this.nameX:this.nameO , row, col, this.squares[i] )
        if (this.winner || this.squares[i]) {
            return;
        }
        const newSquares = this.squares.slice();
        newSquares[i] = this.xIsNext ? 'X' : 'O';
        this.setSquares(newSquares);
        this.setXIsNext(!this.xIsNext);
        this.calculateWinner(newSquares);
        this.handleStopTimer();
    }

    calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                this.setWinnerCombination(lines[i]);
                this.setWinner(squares[a]);
                this.setModalActive(true);
                return;
            }
        }
    }

    handleStopTimer = () => {
        this.setIsGameRunning(this.winner !== null);
    };

    renderSquare(i) {
        let className = "cell";
        if (this.winnerCombination.includes(i)) {
            if (this.winner === "X") {
                className = "cell winX"
            }
            else if (this.winner === "O") {
                className = "cell winO"
            }
        }
        return (
            <MyCell
                className = {className}
                value={this.squares[i]}
                onClick={() => this.handleClick(i)}
            />
        );
    }

    get formattedTime() {
        const minutes = Math.floor(this.seconds / 60);
        const remainingSeconds = this.seconds % 60;
        const formattedTime = `${minutes < 10 ? '0' : ''}${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;

        return formattedTime;
    }

    startTimer() {
        this.isTimerRunning = true;
        this.intervalId = setInterval(() => {
            this.seconds += 1;
        }, 1000);
    }

    stopTimer() {
        this.isTimerRunning = false;
        clearInterval(this.intervalId);
    }

    resetTimer() {
        this.seconds = 0;
        this.isTimerRunning = false;
        clearInterval(this.intervalId);
    }
}

export default  GameStore;
export const GameContext = createContext();
